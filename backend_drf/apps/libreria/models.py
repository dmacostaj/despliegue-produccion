from django.db import models

# Create your models here.

class Autor(models.Model):

    def ruta_de_imagen(instance, filename):                   
        return f'perfil/{instance.nombre}/{filename}' 
    
    nombre = models.CharField( max_length=50)
    imagen = models.ImageField(blank=True, null=True, upload_to=ruta_de_imagen, height_field=None, width_field=None, max_length=None)

    class Meta:
        verbose_name = 'Autor'
        verbose_name_plural = 'Autores'

    def __str__(self):
        return f'{self.id}-{self.nombre}'

    def save(self, *args, **kwargs):
        
        self.nombre = self.nombre.upper()
        super().save(*args, **kwargs)




