from rest_framework import routers
from apps.libreria.api.viewsets import AutorViewSet

router = routers.DefaultRouter()
router.register(r'autor', AutorViewSet,basename="autor")

urlpatterns = router.urls