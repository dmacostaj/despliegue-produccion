from apps.libreria.models import Autor

from rest_framework import serializers
from drf_extra_fields.fields import Base64ImageField

            
class AutorSerializer(serializers.ModelSerializer):
    
    imagen = Base64ImageField(required=False)

    class Meta:
        model = Autor
        fields = ['id', 'nombre', 'imagen']


