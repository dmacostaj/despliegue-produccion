# Despliegue en producción Django Rest Framework - Vue JS - NGINX - GUNICORN con Docker 

## Guía de inicio

```
git clone git@xxxxxxxxxxxxxxxxxxxxxxx
git branch -M main
git push
```

## Inicialiazar el Backend en local

Crear el ambiente virtual
```
python -m venv venv
```
Activar el ambiente virtual
```                         
.\venv\Scripts\activate
```
instalar los requerimientos del proyecto
```   
pip install -r requirementes.txt            
```
Migrar modelo a la bd
```             
python manage.py migrate                    
```
Correr el server de desarrollo
```
python manage.py runserver                  
```

## Inicialiazar el Frontend en local

Instalar paquetes de node
```
yarn
```
Correr el server de desarrollo
```
yarn dev 
```
Compilar carpeta dist para producción
```
yarn build 
```
